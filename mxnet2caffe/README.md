#  mxnet model convert to caffe modle
#### · Reference
	https://github.com/GarrickLin/MXNet2Caffe.git
	Thanks very much!
### · Run env
	docker deepo
### · Convert cmd
	python3 mxnet2caffe.py --config ./insightface_facefeature/net_inst.txt
### · 注意
	1 net_inst.txt的mxnet模型命名有严格要求:
	(1) json文件以-symbol.json结束
	(2) params文件以-0200.params结束，任意的4位数字，笔者试过-0100和-0100,转换输出相同
	(3) mxnet_checkpoints是-symbol和-0200前面的相同字段
	2 caffe的模型维度在prototxt_basic.py中手动修改，也可以生成caffe模型后，修改.prototxt文件
	3 不同的模型会遇到不同问题，因为有些mxnet支持的层在caffe上就不支持了,也可以做一些转换，需要具体问题具体分析。下面是一些经验和问题：
	(1) mxnet的Pooling转成caffe的池化层，如果"global_pool": "True"，则"kernel": "(7, 7)",是失效的，所以在caffe中不需要kernel_size, 增加属性global_pooling: true即可
	(2) mxnet支持的运算比较多，比如"op": "elemwise_mul", "op": "sum", "op": "sqrt", "op": "mean", "op": "broadcast_div", "op": "tile"等等，目前此工程还不支持这些op。
	2 prototxt_basic.py文件中，caffe模型的input直接写死了，转换时要根据实际需求改成相应shape