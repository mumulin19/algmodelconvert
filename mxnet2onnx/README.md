#  mxnet model convert to caffe modle
### · Run env
	docker deepo
	pip3 install onnx-simplifier
### · Convert cmd
	python3 mxnet2onnx.py --config ./insightface_facefeature/net_inst.txt
	python3 -m onnxsim ./insightface_facefeature/inst/insightface_facefeature.onnx ./insightface_facefeature/inst/insightface_facefeature_sim.onnx
### · 注意
#### 1 转换报错：Unrecognized attribute: spatial for operator BatchNormalization
错误原因：mxnet代码的bug。mxnet中的BN，针对的是channel维度。在onnx中，对于version 7中的BN存在spatial参数（可选0、1，默认是1），spatial=1的实现方式正是mxnet中BN的实现方式。在version 9之后，onnx的BN去掉spatial参数，BN的实现方式就是旧版的spatial=1的方式。
在我们的mxnet环境中，将BN转换为onnx中的BatchNormalization时，spatial=0是错误的，并且我们onnx环境中还舍弃了spatial参数。mxnet官方于2020-04-08修正了该问题。
[Fixing spatial export for batchnorm](https://github.com/apache/incubator-mxnet/commit/79c576b8157539d365cc9e0e1e355d4ca12f7374) 
#### 2 执行onnxsim报错，还未寻找原因