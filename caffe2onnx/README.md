#  mxnet model convert to caffe modle
### · Run env
	docker deepo
### · Convert cmd
	python3 convert2onnx.py --config ./libfacedetection/net_inst.txt
### · 注意
	1 libfacedetection库使用的是SSD架构，SSD架构的caffe有个Normalize，BVLC的caffe1.0.0是不支持Normalize的，可以修改源码加上。但是我们本次只是为了验证一下此模型的效率，所以先去掉了Normalize层。
