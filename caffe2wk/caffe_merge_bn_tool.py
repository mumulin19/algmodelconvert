import os
import sys
import argparse
import logging

import numpy as np
import caffe
from caffe.proto import caffe_pb2
from google.protobuf import text_format

bn_maps = {}

def find_top_after_bn(layers, name, top):
    bn_maps[name] = {}
    for l in layers:
        if len(l.bottom) == 0:
            continue
        if l.bottom[0] == top and l.type == "BatchNorm":
            bn_maps[name]["bn"] = l.name
            bn_maps[name]["eps"] = l.batch_norm_param.eps
            bn_maps[name]["moving_average_fraction"] = l.batch_norm_param.moving_average_fraction
            top = l.top[0]
        if l.bottom[0] == top and l.type == "Scale":
            bn_maps[name]["scale"] = l.name
            top = l.top[0]
    return top


def pre_process(expected_proto, new_proto):
    net_specs = caffe_pb2.NetParameter()
    net_specs2 = caffe_pb2.NetParameter()
    with open(expected_proto, "r") as fp:
        text_format.Merge(str(fp.read()), net_specs)

    net_specs2.MergeFrom(net_specs)
    layers = net_specs.layer
    num_layers = len(layers)

    for i in range(num_layers - 1, -1, -1):
        del net_specs2.layer[i]

    for idx in range(num_layers):
        l = layers[idx]
        if (l.type == "BatchNorm" or l.type == "Scale") : #and (not "fc1" in l.name):
            continue
        elif l.type == "Convolution" or l.type == "Deconvolution":
            top = find_top_after_bn(layers, l.name, l.top[0])
            bn_maps[l.name]["type"] = l.type
            layer = net_specs2.layer.add()
            layer.MergeFrom(l)
            layer.top[0] = top
            layer.convolution_param.bias_term = True
        else:
            layer = net_specs2.layer.add()
            layer.MergeFrom(l)

    with open(new_proto, "w") as fp:
        fp.write("{}".format(net_specs2))


def load_weights(net, nobn):
    if sys.version_info > (3, 0):
        listKeys = nobn.params.keys()
    else:
        listKeys = nobn.params.iterkeys()
    for key in listKeys:
        if type(nobn.params[key]) is caffe._caffe.BlobVec:
            conv = net.params[key]
            if key not in bn_maps or "bn" not in bn_maps[key]:
                for i, w in enumerate(conv):
                    nobn.params[key][i].data[...] = w.data
            else:
                print(key)
                bn = net.params[bn_maps[key]["bn"]]
                eps = bn_maps[key]["eps"]
                scale = net.params[bn_maps[key]["scale"]]
                wt = conv[0].data
                channels = 0
                if bn_maps[key]["type"] == "Convolution":
                    channels = wt.shape[0]
                elif bn_maps[key]["type"] == "Deconvolution":
                    channels = wt.shape[1]
                else:
                    print("error type " + bn_maps[key]["type"])
                    exit(-1)
                bias = np.zeros(channels)
                if len(conv) > 1:
                    bias = conv[1].data
                mean = bn[0].data
                var = bn[1].data
                scalef = bn[2].data

                scales = scale[0].data
                shift = scale[1].data

                if scalef != 0:
                    scalef = 1. / scalef
                mean = mean * scalef
                var = var * scalef
                rstd = 1. / np.sqrt(var + eps)
                if bn_maps[key]["type"] == "Convolution":
                    rstd1 = rstd.reshape((channels, 1, 1, 1))
                    scales1 = scales.reshape((channels, 1, 1, 1))
                    wt = wt * rstd1 * scales1
                else:
                    rstd1 = rstd.reshape((1, channels, 1, 1))
                    scales1 = scales.reshape((1, channels, 1, 1))
                    wt = wt * rstd1 * scales1
                bias = (bias - mean) * rstd * scales + shift

                nobn.params[key][0].data[...] = wt
                nobn.params[key][1].data[...] = bias

if __name__ == '__main__':
    #caffe_prototxt = "./headpose/caffe/headpose.prototxt"
    #caffe_model = "./headpose/caffe/headpose.caffemodel"
    #caffe_prototxt = "./yolov3/caffe/yoloface-500k-v2.prototxt"
    #caffe_model = "./yolov3/caffe/yoloface-500k-v2.caffemodel"
    caffe_prototxt = "./insightface_facefeature/caffe/insightface_facefeature.prototxt"
    caffe_model = "./insightface_facefeature/caffe/insightface_facefeature.caffemodel"

    caffe_merge_bn_prototxt = "./" + caffe_prototxt.split(".")[-2] + "_merge_bn.prototxt"
    caffe_merge_bn_model = "./" + caffe_model.split(".")[-2] + "_merge_bn.caffemodel"

    print("caffe prototxt:", caffe_merge_bn_prototxt)
    print("caffe model:", caffe_merge_bn_model)
    pre_process(caffe_prototxt, caffe_merge_bn_prototxt)
    net = caffe.Net(caffe_prototxt, caffe_model, caffe.TEST)
    net2 = caffe.Net(caffe_merge_bn_prototxt, caffe.TEST)

    load_weights(net, net2)
    net2.save(caffe_merge_bn_model)