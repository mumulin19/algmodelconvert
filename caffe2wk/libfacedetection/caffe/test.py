import caffe

root = './'
deploy=root + 'yufacedetectnet-open-v2-2.prototxt'    #deploy文件
caffe_model=root + 'yufacedetectnet-open-v2.caffemodel'   #训练好的 caffemodel
net = caffe.Net(deploy,caffe_model,caffe.TEST)   #加载model和network

#查看各层的参数值，其中k表示层的名称，v[0].data就是各层的W值，而v[1].data是各层的b值。注意：并不是所有的层都有参数，只有卷积层和全连接层才有。
[(k,v[0].data) for k,v in net.params.items()]
#也可以不查看具体值，只想看一下shape,可用命令
[(k,v[0].data.shape) for k,v in net.params.items()]
#假设我们知道其中第一个卷积层的名字叫'Convolution1', 则我们可以提取这个层的参数：
#w1=net.params['Convolution1'][0].data
#b1=net.params['Convolution1'][1].data

#net.forward()   #运行测试

#[(k,v.data.shape) for k,v in net.blobs.items()]  #查看各层数据规模
#fea=net.blobs['InnerProduct1'].data   #提取某层数据（特征）
