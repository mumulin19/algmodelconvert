#  caffe model convert to nnie wk modle
### · 工具
    nnie_mapper_11 : hi3559a转换工具
### · Convert cmd
	./nnie_mapper_11 libfacedetection/net_inst.cfg
### · Run env install
#### 1 安装opencv3.4.0
1.1 下载源码：https://opencv.org/releases/
1.2 编译安装

```
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D WITH_GPHOTO2=OFF -D WITH_GTK=OFF ../
make -j8
sudo make install
```

1.3 修改环境变量

```
vi .bashrc
export PATH=/usr/local/bin/:$PATH
export LD_LIBRARY_PATH=/usr/local/lib/:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig/
```

注意，这里的opencv使用的是默认的gcc和gcc编译的，版本是7.5
#### 2 安装protobuf
2.1 下载源码：https://github.com/protocolbuffers/protobuf/tags，找到3.5.1，官方系统版本是ubuntu14.04，用的是2.5.0，经验证2.5.0在ubuntu18.04上有问题。
2.2 安装gcc4.8
protobuf必须使用gcc4.8和g++4.8版本

```
sudo apt-get install gcc-4.8
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.8 10
```

```
sudo apt-get install g++-4.8
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.8 10
```
2.3 编译安装

```
mkdir build
cd build
cmake -Dprotobuf_BUILD_TESTS=OFF -Dprotobuf_BUILD_SHARED_LIBS=ON ../cmake
make -j8
sudo make install
```
安装路径也是默认的/usr/local，不需要再修改环境变量。
至此，nnie_mapper可以正常执行了。